import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.lineSeparator;

public class TestMulChoise {
    @Test
    void showIfTheAnswerIsCorrectMulChoice() {
        ArrayList<MulChoise> listQuestions = new ArrayList<>();
      /*  ArrayList<String> opt = new ArrayList<>();
        opt.add("L’échelle de Trilling\n");
        opt.add("L’échelle de Richter\n");
        opt.add("L’échelle de Beaufort\n"); */

        listQuestions.add(new MulChoise("Sur quelle échelle est exprimée l’intensité des tremblements de terre ?",
                List.of("1. L’échelle de Trilling" + lineSeparator() + "2. L’échelle de Richter" + lineSeparator() +
                        "3. L’échelle de Beaufort"),"L’échelle de Richter", "2" , 3));
        listQuestions.add(new MulChoise("À combien d’œufs de poule équivaut un œuf d’autruche ?", List.of("5" +
                lineSeparator() + "15" + lineSeparator() + "25"), "25", "3", 3));

        Assertions.assertEquals(true, listQuestions.get(0).tryAnswer(listQuestions.get(0).getAnsw()));
        //Je profite pour tester getQues
        Assertions.assertEquals("C'est une question à choix multiple\n" + "Sur quelle échelle est exprimée " +
                "l’intensité des tremblements de terre ?" + lineSeparator() +
                "1. L’échelle de Trilling" + lineSeparator() + "2. L’échelle de Richter" + lineSeparator() +
                "3. L’échelle de Beaufort", listQuestions.get(0).getQues());

    }
}

