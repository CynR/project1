import org.apache.commons.text.similarity.LevenshteinDistance;
import java.text.Normalizer;

public class Question {
    protected String ques;
    protected String answer;
    protected int valeur;

    public Question(String q, String a, int v) {
        this.ques = q;
        this.answer = a;
        this.valeur = v;
    }

    public String getQues() {
        return getTextQues() + ques;
    }

    public String getTextQues(){
        return ("C'est une question ouverte\n" );
    }

    public String getAnsw(){
        return answer;
    }

    public int getValue() {return valeur;}

    public Boolean tryAnswer(String userAnswer){
        //StringUtils ansUnaccent = new StringUtils();
        LevenshteinDistance lev = new LevenshteinDistance();
        lev.apply(answer.toLowerCase(),userAnswer.toLowerCase());

        if(lev.apply(answer.toLowerCase(),userAnswer.toLowerCase())<4) {
            return true;
        }
        ;
        return false;
    }

    class StringUtils {

        public String unaccent(String src) {
            return Normalizer
                    .normalize(src, Normalizer.Form.NFD)
                    .replaceAll("[^\\p{ASCII}]", "");
        }

    }
}