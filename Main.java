import java.util.*;
import java.util.ArrayList;
import static java.lang.System.lineSeparator;

public class Main {
    public static void main(String[] args) {
        ArrayList<Question> listQuestions = new ArrayList<>();

        listQuestions.add(new MulChoise("Sur quelle échelle est exprimée l’intensité des tremblements de terre ?",
                List.of("1. L’échelle de Trilling" + lineSeparator() + "2. L’échelle de Richter" + lineSeparator() +
                        "3. L’échelle de Beaufort"),"L’échelle de Richter", "2" , 3));
        //"Est-ce que la réponse est '206'" + "Ecrivez VRAI ou FAUX"
        listQuestions.add(new VvsF("Il y en a 206 os qui constituent le squelette d’un homme ou d’une femme adulte ",
                "VRAI", 3));
        listQuestions.add(new Question("Laïka était le premier animal quia fait le tour de la Terre dans un " +
                "engin spatial. Quel animal était Laïka ?", "Un chien", 3));
        listQuestions.add(new VvsF("L’Oscar la statuette qui est remise chaque année à Hollywood " +
                "pour le meilleur film ?", "VRAI", 1));
        listQuestions.add(new VvsF("L’organisation qui aide les enfants dans le monde entier est L'UNOCEF",
                "FAUX", 2));
        listQuestions.add(new Question("Avec quelle lettre les Romains représentaient-ils le chiffre 500 ?",
                "D", 1));
        listQuestions.add(new Question("En Afrique, on trouve des chutes d’eau qui portent un prénom féminin. Lequel ?",
                "Victoria", 2));
        listQuestions.add(new Question("Parquel village anglais passe le méridien 0 ?", "Greenwich", 2));
        listQuestions.add(new VvsF("Il y en a 12 quilles sur la piste de bowling", "FAUX", 1));
        listQuestions.add(new MulChoise("À combien d’œufs de poule équivaut un œuf d’autruche ?", List.of("5" +
                lineSeparator() + "15" + lineSeparator() + "25"), "25","3" , 3));

        Play players = new Play();
        int roundsPlayers = players.listPlayers.size();
        String userAnswer = "";
        //rounds for each player
        int scoreMaxGame = 0;
        for(int x = 0; x < roundsPlayers; x++) {
            System.out.println("C'est le tour de " + (Play.listPlayers.get(x)).getNamePlayer() + lineSeparator());
            //get question, try it, update score
            for (Question q : listQuestions) {
                if(x == 0) {
                    scoreMaxGame = scoreMaxGame + q.getValue();
                }
                System.out.println(q.getQues());
                Scanner anUser = new Scanner(System.in);
                userAnswer = anUser.nextLine();
                if (q.tryAnswer(userAnswer) == true) {
                    System.out.println("Bravo, good answer!");
                    Play.listPlayers.get(x).setScore(q.getValue());
                    System.out.println("You have earned " + q.getValue());
                    System.out.println("Your score now is " + (Play.listPlayers.get(x)).getScore() +
                            lineSeparator());
                } else {
                    System.out.println("Shame on you! " + "Try harder next time" + lineSeparator());
                }
            }
        }
        //determine the highest score between the players and the winner
        if(roundsPlayers>=1) {
            int scoreMax = 0;
            String winner = "";
            for (int x = 0; x < roundsPlayers; x++){
                if((Play.listPlayers.get(x)).getScore() > scoreMax) {
                    scoreMax = (Play.listPlayers.get(x).getScore());
                    winner = (Play.listPlayers.get(x).getNamePlayer());
                }
            }
            //shows message for winner and his score
            if (roundsPlayers > 1) {
                System.out.println("End of game ");
                System.out.println();
                System.out.println(" The winner is " + winner + " with " + scoreMax + " points :) ");
            }
            //if there was just 1 player
            else if ((roundsPlayers > 0) && (roundsPlayers < 2)){
                if(scoreMax > 0 )
                    System.out.println("Bravo " + winner + " you have finished with " +
                            scoreMax + " from maximum of " + scoreMaxGame);
                else
                    System.out.println("You answer wrong all the questions ...");
                }
            }

        }


}



