import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.lang.System.currentTimeMillis;
import static java.lang.System.lineSeparator;

public class VvsF extends Question {

    public VvsF(String q, String a, int v) {
        super(q, a, v);
    }

    public String getQues() {
        return getTextQues() + ques;
    }

    public String getTextQues(){
        return ("C'est une question à VRAI ou FAUX\n" );
    }

    public int getValue() {
        return valeur;
    }

    public String getAnsw(){
        return answer;
    }

    public Boolean tryAnswer(String userAnswer) {
        boolean actual = true;
        List<String> optionsTrue = new ArrayList<>();
        Collections.addAll(optionsTrue,"VRAI","TRUE","OUI","SI");
        List<String> optionsFalse = new ArrayList<>();
        Collections.addAll(optionsFalse, "FAUX", "FALSE", "NON", "NO");

        if(answer.equals("VRAI")) {
            for (String s : optionsTrue) {
                if (userAnswer.toLowerCase().equals(s.toLowerCase())) {
                    actual = true;
                    break;
                }
                else actual = false;
            }
        }
        else if (answer.equals("FAUX")){
            for (String s : optionsFalse) {
                if (userAnswer.toLowerCase().equals(s.toLowerCase())) {
                    actual = true;
                    break;
                }
                else actual = false;
            }
        }
        return actual;
    }
}


