import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class TestVsvF {
    @Test
    void showIfTheAnswerIsCorrectVF(){
        ArrayList<VvsF> listQuestions = new ArrayList<>();
        listQuestions.add(new VvsF("Il y en a 206 os qui constituent le squelette d’un homme ou d’une femme adulte ",
                "VRAI", 3));
        listQuestions.add(new VvsF("L’organisation qui aide les enfants dans le monde entier est L'UNOCEF",
                "FAUX", 2));

        Assertions.assertEquals(true, listQuestions.get(0).tryAnswer("SI"));
        Assertions.assertEquals(true, listQuestions.get(1).tryAnswer("FALSE"));
        Assertions.assertEquals(true, listQuestions.get(1).tryAnswer(listQuestions.get(1).getAnsw()));
    }
}
