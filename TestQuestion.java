import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class TestQuestion {

    @Test
    void showQuestion(){
        ArrayList<Question> listQuestions = new ArrayList<>();
        listQuestions.add(new Question("Laïka était le premier animal quia fait le tour de la Terre dans un " +
                "engin spatial. Quel animal était Laïka ?", "Un chien", 3));
        listQuestions.add(new Question("Avec quelle lettre les Romains représentaient-ils le chiffre 500 ?",
                "D", 1));

        Assertions.assertEquals("C'est une question ouverte\n" + "Laïka était le premier animal quia fait le tour de la Terre dans un " +
                "engin spatial. Quel animal était Laïka ?", listQuestions.get(0).getQues());

    }

   @Test
    void showIfTheAnswerIsCorrect(){
        ArrayList<Question> listQuestions = new ArrayList<>();
        listQuestions.add(new Question("Laïka était le premier animal quia fait le tour de la Terre dans un " +
                "engin spatial. Quel animal était Laïka ?", "Un chien", 3));
        listQuestions.add(new Question("Avec quelle lettre les Romains représentaient-ils le chiffre 500 ?",
                "D", 1));

       Assertions.assertEquals(true, listQuestions.get(0).tryAnswer(listQuestions.get(0).getAnsw()));
       Assertions.assertEquals(true, listQuestions.get(0).tryAnswer("CHIEN"));
    }

}

