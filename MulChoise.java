import java.util.List;
import java.util.*;
import static java.lang.System.lineSeparator;

public class MulChoise extends Question {

    List<String> options = new ArrayList<>();
    String correctOpt;

    public MulChoise(String q, List<String> answerOptions, String answer, String correctOptions, int v) {
        super(q, answer, v);
        this.options = answerOptions;
        this.correctOpt = correctOptions;
    }

    public String getQues() {
        return getTextQues() + ques + lineSeparator() + listWithoutBrackets();
    }
    //afficher la list wthout , et []
      public String listWithoutBrackets(){
        String optSansCrochets = Arrays.toString(options.toArray()).replace(", N", "N").replace(", N", "N");
        return optSansCrochets.substring(1,optSansCrochets.length()-1);
    }

    public String getTextQues(){
       return ("C'est une question à choix multiple\n" );
    }

    public int getValue() {
        return valeur;
    }

    public String getAnsw(){
        return answer;
    }

    public Boolean tryAnswer(String userAnswer){
        //StringUtils ansUnaccent = new StringUtils();
        if((answer.toLowerCase().equals(userAnswer.toLowerCase())) || (correctOpt.toLowerCase().equals(userAnswer.toLowerCase())) ) {
            return true;
        }
        ;
        return false;
    }
}
